/*
 * Copyright (c) 2018 - 2020, Entgra (Pvt) Ltd. (http://www.entgra.io) All Rights Reserved.
 *
 * Entgra (Pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.wso2.carbon.device.mgt.common;

import org.wso2.carbon.device.mgt.common.operation.mgt.Operation;

/**
 * This class holds required parameters for a querying a paginated activity response.
 */
public class ActivityPaginationRequest {

    private int offset;
    private int limit;
    private String deviceType;
    private String deviceId;
    private String operationCode;
    private String initiatedBy;
    private long since;
    private Operation.Type type;
    private Operation.Status status;

    public ActivityPaginationRequest(int offset, int limit) {
        this.offset = offset;
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public long getSince() {
        return since;
    }

    public void setSince(long since) {
        this.since = since;
    }


    public Operation.Type getType() {
        return type;
    }

    public void setType(Operation.Type type) {
        this.type = type;
    }

    public Operation.Status getStatus() {
        return status;
    }

    public void setStatus(Operation.Status status) {
        this.status = status;
    }

}
